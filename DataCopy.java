package pxl;
import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.*;
public class DataCopy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame f= new JFrame();
		JPanel panel1= new JPanel();
		JPanel panel2= new JPanel();
		panel1.setPreferredSize(new Dimension(180, 100));
		panel2.setPreferredSize(new Dimension(180, 250));
		f.setTitle("Data Copy");
		f.setSize(800,800);
		f.setLocation(300,200);//setting location of the window 
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.getContentPane().add(panel1, BorderLayout.PAGE_START);
		f.getContentPane().add(panel2, BorderLayout.SOUTH);
		JPanel panel = new JPanel();
       JLabel jl = new JLabel();
       JTextField jt = new JTextField(3);
       jt.setBounds(50,150, 200,50);  
       JButton jb = new JButton("Enter");
       
        panel.setLayout(new BorderLayout());
        panel.setOpaque(true);
        JButton submit = new JButton("Submit");
        panel.add(jt,BorderLayout.NORTH);


        jt.addActionListener(new ActionListener()
        {
               public void actionPerformed(ActionEvent e)
               {
                     String input = jt.getText();
                     jl.setText(input);
                     System.out.println(input);
               }
        });
        panel.add(jb,BorderLayout.SOUTH);
        jb.addActionListener(new ActionListener()
        {
                public void actionPerformed(ActionEvent e)
                {
                       String input = jt.getText();
                       jl.setText(input);
                }
        });
        panel.add(jl,BorderLayout.CENTER);
        panel1.add(panel);
        panel2.add(submit);
//        f.setContentPane(panel);
        f.pack();
        f.setVisible(true);
	}

}
